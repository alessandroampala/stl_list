#pragma once

template<class T>
struct is_iterator
{
	static T makeT();
	typedef void* twoptrs[2];  // sizeof(twoptrs) > sizeof(void *)
	static twoptrs& test(...); // Common case
	template<class R> static typename R::iterator_category* test(R); // Iterator
	template<class R> static void* test(R*); // Pointer

	static const bool value = sizeof(test(makeT())) == sizeof(void*);
};