# stl_list
A custom forward_list implementation compliant to the STL interface:

- Offers all methods that are offered by std::forward_list
- **Header-only** library
- Exposes both **iterator** and **const_iterator**
- Comes with a templated test class ready to test this as well as different implementations, and a SFINAE flavoured struct to test if a class is an iterator