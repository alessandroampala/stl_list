#pragma once

#include <iterator>
#include "Node.h"
#include "is_iterator.h"

template<typename T>
class SListIterator : public std::iterator<std::forward_iterator_tag, Node<T>>
{
public:
	SListIterator(Node<T>* x) :p(x) {}
	SListIterator(const SListIterator& it) : p(it.p) {}
	SListIterator& operator++() { p = p->next; return *this; }
	SListIterator operator++(int) { SListIterator tmp(*this); operator++(); return tmp; }
	bool operator==(const SListIterator& rhs) const { return p == rhs.p; }
	bool operator!=(const SListIterator& rhs) const { return p != rhs.p; }
	T& operator*() const { return p->data; }
private:
	Node<T>* p;
	
	template<typename T>
	friend class SList;
	template<typename T>
	friend class const_SListIterator;
};

template<typename T>
class const_SListIterator : public std::iterator<std::forward_iterator_tag, Node<T>>
{
public:
	const_SListIterator(Node<T>* x) :p(x) {}
	const_SListIterator(const const_SListIterator& it) : p(it.p) {}
	const_SListIterator(SListIterator<T>& it) : p(it.p) {}
	const_SListIterator& operator++() { p = p->next; return *this; }
	const_SListIterator operator++(int) { const_SListIterator tmp(*this); operator++(); return tmp; }
	bool operator==(const const_SListIterator& rhs) const { return p == rhs.p; }
	bool operator!=(const const_SListIterator& rhs) const { return p != rhs.p; }
	const T& operator*() const { return p->data; }
private:
	Node<T>* p;

	template<typename T>
	friend class SList;
};


template<typename T>
class SList
{
public:

	using iterator = SListIterator<T>;
	using const_iterator = const_SListIterator<T>;

	SList(std::initializer_list<T> _Ilist)
	{
		assign(_Ilist.begin(), _Ilist.end());
	}

	SList()
	{
	}

	SList<T>& operator=(std::initializer_list<T> _Ilist) {
		assign(_Ilist.begin(), _Ilist.end());
		return *this;
	}

	//Iterators

	iterator before_begin()
	{
		return iterator(new Node<T>(T(), m_head));
	}

	iterator begin()
	{
		return iterator(m_head);
	}

	iterator end()
	{
		return iterator(nullptr);
	}

	//BUGGED
	const_iterator cbefore_begin()
	{
		return const_iterator(new Node<T>(T(), m_head));
	}

	const_iterator cbegin()
	{
		return const_iterator(m_head);
	}

	const_iterator cend()
	{
		return const_iterator(nullptr);
	}

	//Capacity

	bool empty()
	{
		return m_head == nullptr;
	}

	//Element access

	T& front()
	{
		return m_head->data;
	}

	//Using SFINAE to select this function only when InputIterator is an iterator
	template <class InputIterator, std::enable_if_t<is_iterator<InputIterator>::value, bool> = 0>
	void assign(InputIterator first, InputIterator last)
	{
		clear();

		Node<T>* p = new Node<T>(*first, nullptr);
		m_head = p;
		++first;

		while (first != last)
		{
			p->next = new Node<T>(*first, nullptr);
			p = p->next;
			++first;
		}
	}

	void assign(std::size_t n, const T& val)
	{
		clear();
		if (n == 0) return;

		Node<T>* p = new Node<T>(val, nullptr);
		m_head = p;
		if (n == 1) return;

		for (unsigned int i = 1; i < n; ++i)
		{
			p->next = new Node<T>(val, nullptr);
			p = p->next;
		}
	}

	

	void assign(std::initializer_list<T> il)
	{
		clear();
		auto it = il.begin();
		if (it == il.end()) return;
		Node<T>* p = new Node<T>(*it, nullptr);
		++it;
		m_head = p;

		for (; it != il.end(); ++it)
		{
			p->next = new Node<T>(*it, nullptr);
			p = p->next;
		}
	}

	template <class... Args>
	void emplace_front(Args&&... args)
	{
		T elem(args...);
		Node<T>* newNode = new Node<T>(elem, m_head);
		m_head = newNode;
	}

	void push_front(const T& val)
	{
		Node<T>* newNode = new Node<T>(val, m_head);
		m_head = newNode;
	}

	void pop_front()
	{
		Node<T>* oldHead = m_head;
		m_head = m_head->next;
		delete oldHead;
	}

	template <class... Args>
	iterator emplace_after(const_iterator position, Args&&... args)
	{
		T elem(args...);

		if (position.p->next == m_head)
		{
			if(empty())
				m_head = new Node<T>(elem, nullptr);
			else
				m_head = new Node<T>(elem, m_head->next);
			return iterator(m_head);
		}

		Node<T>* after = position.p->next;
		position.p->next = new Node<T>(elem, after);
		return iterator(position.p->next);
	}

	iterator insert_after(const_iterator position, const T& val)
	{
		bool adjustHead = false;
		if (position.p->next == m_head)
			adjustHead = true;
		Node<T>* after = position.p->next;
		position.p->next = new Node<T>(val, after);
		if (adjustHead)
			m_head = position.p->next;
		return iterator(position.p->next);
	}

	iterator insert_after(const_iterator position, T&& val)
	{
		bool adjustHead = false;
		if (position.p->next == m_head)
			adjustHead = true;

		Node<T>* after = position.p->next;
		position.p->next = new Node<T>(val, after);
		if (adjustHead)
			m_head = position.p->next;
		return iterator(position.p->next);
	}

	iterator insert_after(const_iterator position, std::size_t n, const T& val)
	{
		bool adjustHead = false;
		if (position.p->next == m_head)
			adjustHead = true;

		Node<T>* after = position.p->next;
		Node<T>* p = position.p;

		for (unsigned int i = 0; i < n; ++i)
		{
			p->next = new Node<T>(val, nullptr);
			if (adjustHead)
			{
				m_head = p->next;
				adjustHead = false;
			}
			p = p->next;
		}
		p->next = after;
		return iterator(p);
	}

	//Using SFINAE to select this function only when InputIterator is an iterator
	template <class InputIterator, std::enable_if_t<is_iterator<InputIterator>::value, bool> = 0>
	iterator insert_after(const_iterator position, InputIterator first, InputIterator last)
	{
		bool adjustHead = false;
		if (position.p->next == m_head)
			adjustHead = true;

		Node<T>* after = position.p->next;
		Node<T>* p = position.p;

		while (first != last)
		{
			p->next = new Node<T>(*first, nullptr);
			if (adjustHead)
			{
				m_head = p->next;
				adjustHead = false;
			}
			p = p->next;
			++first;
		}
		p->next = after;
		return iterator(p);
	}

	iterator insert_after(const_iterator position, std::initializer_list<T> il)
	{
		bool adjustHead = false;
		if (position.p->next == m_head)
			adjustHead = true;

		Node<T>* after = position.p->next;
		Node<T>* p = position.p;
		for (auto it = il.begin(); it != il.end(); ++it)
		{
			p->next = new Node<T>(*it, nullptr);

			if (adjustHead)
			{
				m_head = p->next;
				adjustHead = false;
			}

			p = p->next;
		}
		p->next = after;
		return iterator(p);
	}

	iterator erase_after(const_iterator position)
	{
		Node<T>* begin = position.p;
		if (position.p->next)
		{
			Node<T>* p = position.p->next;
			position.p->next = position.p->next->next;
			delete p;
			return iterator(position.p->next);
		}

		return iterator(nullptr);
	}

	iterator erase_after(const_iterator position, const_iterator last)
	{
		bool adjustHead = false;
		if (position.p->next == m_head) //before_begin case
		{
			adjustHead = true;
		}

		if (position == last) return iterator(last.p);

		Node<T>* begin = position.p;
		++position;

		while (position != last)
		{
			Node<T>* p = position.p;
			++position;
			delete p;
		}

		if (adjustHead)
			m_head = last.p;
		else
			begin->next = last.p;
		return iterator(last.p);
	}

	void swap(SList<T>& list)
	{
		Node<T>* tmp = m_head;
		m_head = list.m_head;
		list.m_head = tmp;
	}

	void resize(std::size_t n)
	{
		resize(n, T());
	}

	void resize(std::size_t n, const T& val)
	{
		Node<T>* p = m_head;
		unsigned int i = 0;
		if (m_head)
		{
			//advance the pointer since the last node or the nth node
			for (i = 1; i < n && p->next; ++i)
			{
				p = p->next;
			}
		}

		if (i < n) //list smaller than n (p->next == nullptr)
		{
			insert_after(const_iterator(p), n - i, val);
		}
		else //list bigger than n
		{
			erase_after(const_iterator(p), cend());
		}
	}

	void clear()
	{
		Node<T>* n = m_head;

		while (n != nullptr)
		{
			Node<T>* tmp = n;
			n = n->next;
			delete tmp;
		}
		m_head = nullptr;
	}

	//Operations

	void splice_after(const_iterator position, SList<T>& lst)
	{
		insert_after(position, lst.cbegin(), lst.cend());
		lst.clear();
	}

	void splice_after(const_iterator position, SList<T>&& lst)
	{
		insert_after(position, lst.cbegin());
		lst.clear();
	}

	void splice_after(const_iterator position, SList<T>& lst, const_iterator i)
	{
		const_iterator it = i;
		++it;
		insert_after(position, *it);
		const_iterator b(i.p);
		lst.erase_after(b);
	}

	void splice_after(const_iterator position, SList<T>&& lst, const_iterator i)
	{
		const_iterator it = i;
		++it;
		insert_after(position, *it);
		const_iterator b(i.p);
		lst.erase_after(b, ++i);
	}

	void splice_after(const_iterator position, SList<T>& lst,
		const_iterator first, const_iterator last)
	{
		const_iterator it = first;
		++it;
		insert_after(position, it, last);
		lst.erase_after(first, last);
	}

	void splice_after(const_iterator position, SList<T>&& lst,
		const_iterator first, const_iterator last)
	{
		const_iterator it = first;
		++it;
		insert_after(position, it, last);
		lst.erase_after(first, last);
	}

	void remove(const T& val)
	{
		auto equalPred = [&](const T& elem) {return elem == val; };
		remove_if(equalPred);
	}

	template <class Predicate>
	void remove_if(Predicate pred)
	{
		Node<T>* p = m_head;
		//remove initial values and update m_head
		while (p && m_head == p && pred(p->data))
		{
			p = p->next;
			delete m_head;
			m_head = p;
		}
		if (!p) return;
		while (p->next)
		{
			if (pred(p->next->data))
			{
				Node<T>* tmp = p->next;
				p->next = p->next->next;
				delete tmp;
			}
			else
				p = p->next;
		}
	}

	void unique()
	{
		auto equalPred = [](const T& a, const T& b) {return a == b; };
		unique(equalPred);
	}

	template <class BinaryPredicate>
	void unique(BinaryPredicate binary_pred)
	{
		const_iterator first(m_head);
		const_iterator after = first;
		++after;
		while (after != cend())
		{
			if (binary_pred(*first, *after))
			{
				erase_after(first, ++after);
			}
			else
			{
				first = after;
				++after;
			}
		}
	}

	void reverse()
	{
		if (!m_head || !m_head->next) return;

		Node<T>* newList = new Node<T>(m_head->data, nullptr);
		for (Node<T>* p = m_head->next; p; p = p->next)
		{
			newList = new Node<T>(p->data, newList);
		}
		clear();
		m_head = newList;
	}

private:
	Node<T>* m_head = nullptr;
};
