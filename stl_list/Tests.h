#pragma once

#include <assert.h>
#include <forward_list>
#include <array>

// a predicate implemented as a function:
bool single_digit(const int& value) { return (value < 10); }

// a predicate implemented as a class:
class is_odd_class
{
public:
	bool operator() (const int& value) { return (value % 2) == 1; }
} is_odd_object;

// a binary predicate implemented as a function:
bool same_integral_part(double first, double second)
{
	return (int(first) == int(second));
}

// a binary predicate implemented as a class:
class is_near_class
{
public:
	bool operator() (double first, double second)
	{
		return (fabs(first - second) < 5.0);
	}
} is_near_object;

template<template<typename D> class T>
class Test
{
public:

	void runTests()
	{
		testEmpty();
		testFront();
		testAssign();
		testEmplaceFront();
		testPushFront();
		testPopFront();
		testEmplaceAfter();
		testInsertAfter();
		testEraseAfter();
		testSwap();
		testResize();
		testClear();
		testSpliceAfter();
		testRemove();
		testRemoveIf();
		testUnique();
		testReverse();
	}

private:
	void testEmpty()
	{
		T<int> first;
		T<int> second = { 20, 40, 80 };
		assert(first.empty());
		assert(!second.empty());
	}

	void testFront()
	{
		T<int> mylist = { 2, 16, 77 };
		mylist.front() = 11;
		T<int> result = { 11, 16, 77 };
		assert(equals(mylist, result));
	}

	void testAssign()
	{
		T<int> first;
		T<int> second;

		first.assign(4, 15);                           // 15 15 15 15
		second.assign(first.begin(), first.end());     // 15 15 15 15
		first.assign({ 77, 2, 16 });                  // 77 2 16
		assert(equals(first, { 77, 2, 16 }));
		assert(equals(second, { 15, 15, 15, 15 }));
	}

	void testEmplaceFront()
	{
		T< std::pair<int, char> > mylist;

		mylist.emplace_front(10, 'a');
		mylist.emplace_front(20, 'b');
		mylist.emplace_front(30, 'c');

		T< std::pair<int, char> > result = {
			std::pair<int, char>(30, 'c'),
			std::pair<int, char>(20, 'b'),
			std::pair<int, char>(10, 'a'),
		};

		assert(equals(mylist, result));
	}

	void testPushFront()
	{
		T<int> mylist = { 77, 2, 16 };
		mylist.push_front(19);
		mylist.push_front(34);
		T<int> result = { 34, 19, 77, 2, 16 };

		assert(equals(mylist, result));
	}

	void testPopFront()
	{
		T<int> mylist = { 10, 20, 30, 40 };
		T<int> mylist2 = { 10, 20, 30, 40 };

		auto it = mylist2.cbegin();
		while (!mylist.empty())
		{
			assert(*it == mylist.front());
			mylist.pop_front();
			++it;
		}
	}

	void testEmplaceAfter()
	{
		T< std::pair<int, char> > mylist;
		auto cit = mylist.cbefore_begin();
		auto it = mylist.before_begin();

		mylist.emplace_after(cit, 100, 'x');
		cit = mylist.cbefore_begin(); ++cit;
		mylist.emplace_after(cit, 200, 'y');
		cit = mylist.cbefore_begin(); ++cit; ++cit;
		mylist.emplace_after(cit, 300, 'z');

		T< std::pair<int, char> > result = {
			std::pair<int, char>(100, 'x'),
			std::pair<int, char>(200, 'y'),
			std::pair<int, char>(300, 'z'),
		};

		assert(equals(mylist, result));
	}

	void testInsertAfter()
	{
		std::array<int, 3> myarray = { 11, 22, 33 };
		T<int> mylist;
		auto it = mylist.begin();
		auto cit = mylist.cbegin();

		mylist.insert_after(mylist.cbefore_begin(), 10);
		cit = mylist.cbegin();
		mylist.insert_after(cit, 2, 20);
		cit = mylist.cbegin(); ++cit; ++cit;
		mylist.insert_after(cit, myarray.cbegin(), myarray.cend());
		cit = mylist.cbegin();
		mylist.insert_after(cit, { 1,2,3 });

		T<int> result = { 10, 1, 2, 3, 20, 20, 11, 22, 33 };
		assert(equals(mylist, result));
	}

	void testEraseAfter()
	{
		T<int> mylist = { 10, 20, 30, 40, 50 };
													// 10 20 30 40 50
		auto it = mylist.begin();                   // ^

		it = mylist.erase_after(it);                // 10 30 40 50
												    //    ^
		it = mylist.erase_after(it, mylist.cend()); // 10 30
												    //       ^

		T<int> result = { 10, 30 };
		assert(equals(mylist, result));
	}

	void testSwap()
	{
		T<int> first = { 10, 20, 30 };
		T<int> firstCopy = { 10, 20, 30 };
		T<int> second = { 100, 200 };
		T<int> secondCopy = { 100, 200 };

		first.swap(second);

		assert(equals(second, firstCopy));
		assert(equals(first, secondCopy));
	}

	void testResize()
	{
		T<int> mylist = { 10, 20, 30, 40, 50 };
		// 10 20 30 40 50
		mylist.resize(3);             // 10 20 30
		assert(equals(mylist, { 10, 20, 30}));
		mylist.resize(5, 100);         // 10 20 30 100 100
		assert(equals(mylist, { 10, 20, 30, 100, 100 }));
	}

	void testClear()
	{
		T<int> mylist = { 10, 20, 30 };
		mylist.clear();
		assert(equals(mylist, {}));
		mylist.insert_after(mylist.cbefore_begin(), { 100, 200 });
		assert(equals(mylist, { 100, 200 }));
	}

	void testSpliceAfter()
	{
		T<int> first = { 1, 2, 3 };
		T<int> second = { 10, 20, 30 };

		auto it = first.begin();  // points to the 1

		first.splice_after(first.cbefore_begin(), second);
		// first: 10 20 30 1 2 3
		// second: (empty)
		// "it" still points to the 1 (now first's 4th element)
		assert(equals(first, { 10, 20, 30, 1, 2, 3 }));
		assert(equals(second, {}));


		second.splice_after(second.cbefore_begin(), first, first.cbegin(), it);
		// first: 10 1 2 3
		// second: 20 30
		assert(equals(first, { 10, 1, 2, 3 }));
		assert(equals(second, { 20, 30}));

		first.splice_after(first.cbefore_begin(), second, second.cbegin());
		// first: 30 10 1 2 3
		// second: 20
		// * notice that what is moved is AFTER the iterator
		assert(equals(first, { 30, 10, 1, 2, 3 }));
		assert(equals(second, { 20 }));
	}

	void testRemove()
	{
		T<int> mylist = { 10, 20, 30, 40, 30, 20, 10 };
		mylist.remove(20);
		assert(equals(mylist, { 10, 30, 40, 30, 10 }));
	}

	

	void testRemoveIf()
	{
		T<int> mylist = { 7, 80, 7, 15, 85, 52, 6 };

		mylist.remove_if(single_digit);      // 80 15 85 52

		mylist.remove_if(is_odd_object);     // 80 52

		assert(equals(mylist, { 80, 52 }));
	}

	void testUnique()
	{
		T<double> mylist = { 3.14,  3.99, 15.2, 15.2, 15.85,
							   18.5,  69.2,  69.5, 73.0, 73.0 };


		mylist.unique();                     //   3.14,  3.99, 15.2, 15.85
											 //  18.5,  69.2,  69.5, 73.0

		mylist.unique(same_integral_part);  //  3.14, 15.2, 18.5,  69.2, 73.0

		mylist.unique(is_near_object);      //  3.14, 15.2, 69.2

		assert(equals(mylist, { 3.14, 15.2, 69.2 }));
	}

	void testReverse()
	{
		T<int> mylist = { 10, 20, 30, 40 };
		mylist.reverse();
		assert(equals(mylist, { 40, 30, 20, 10 }));
	}

	template<template<typename D> class L, typename D>
	bool equals(L<D> a, L<D> b)
	{
		auto itB = b.cbegin();
		for (auto it = a.cbegin(); it != a.cend(); it++)
		{
			if (*it != *itB)
				return false;
			++itB;
		}
		return true;
	}
};
