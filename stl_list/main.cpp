// stl_list.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include "SList.h"
#include "Tests.h"

int main()
{
	Test<SList> t;
	t.runTests();
	std::cout << "All tests passed!" << std::endl;
}