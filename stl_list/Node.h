#pragma once

template<typename T>
class Node
{
public:
	Node(const T& data, Node<T>* next)
		: data(data), next(next)
	{}

	T data;
	Node<T>* next;
};